FROM node:15.3.0-alpine as build

WORKDIR /app
RUN apk update && apk upgrade && \ 
	apk add bash git openssh

COPY package.json yarn.lock /app/
RUN yarn

COPY . /app

RUN yarn run build

FROM nginx

COPY --from=build /app/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]



