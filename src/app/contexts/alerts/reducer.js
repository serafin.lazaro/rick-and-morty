const AlertReducer = (state, action) => {
    switch (action.type) {
      case "SHOW_MESSAGE":
        return {
          ...state,
          options: action.payload
        };
      default:
        return state;
    }
  };

  export default AlertReducer