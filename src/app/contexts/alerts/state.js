import React, { useContext, useReducer } from "react";
import { AlertContext } from "./context";
import AlertReducer from "./reducer";

export const useAlert = () => {
    const { state, dispatch } = useContext(AlertContext);
    return [state, dispatch];
};

export const AlertState = ({ children }) => {
    const initialState = {
        options: {}
    };

    const [state, dispatch] = useReducer(AlertReducer, initialState);

    return (
        <AlertContext.Provider
            value={{
                state: state,
                dispatch: dispatch
            }}
        >
            {children}
        </AlertContext.Provider>
    );
};