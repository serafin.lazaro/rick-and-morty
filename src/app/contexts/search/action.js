export const searchCharacter = async (dispatch, characterName, url) => {

    let _url = url ? url : `https://rickandmortyapi.com/api/character${characterName && `?name=${characterName}`}`;

    await fetch(_url)
        .then(response =>
            response.json()
        ).then(body => {
            debugger
            dispatch({
                type: "GET_CHARACTERS", payload: body ? body : {
                    info: {}, results: []
                }
            })
        }
        );;
}
