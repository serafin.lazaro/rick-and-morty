import React, { useContext, useReducer } from "react";
import { SearchContext } from "./context";
import CharactersReducer from "./reducer";

export const useSearch = () => {
    const { state, dispatch } = useContext(SearchContext);
    return [state, dispatch];
};

export const SearchState = ({ children }) => {
    const initialState = {
        characters: {
            info: {}, results: []
        }
    };

    const [state, dispatch] = useReducer(CharactersReducer, initialState);

    return (
        <SearchContext.Provider
            value={{
                state: state,
                dispatch: dispatch
            }}
        >
            {children}
        </SearchContext.Provider>
    );
};