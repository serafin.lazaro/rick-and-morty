import { React, useState } from 'react';
import { useSearch } from '../../contexts/search/state';
import { searchCharacter } from '../../contexts/search/action';
import Character from './character'
import CharacterSelected from './characterSelected'


function Dashboard() {
    const [characterName, setCharacterName] = useState('');
    const [characterSelected, setCharacterSelected] = useState({});
    const [searchState, dispatch] = useSearch();
    const { characters } = searchState;
    const [isLoading, setIsLoading] = useState(false);

    const handleChange = (event) => {
        setCharacterName(event.target.value);
    }

    const handleSubmit = (event, url) => {
        setIsLoading(true);
        searchCharacter(dispatch, characterName, url).then(() => setIsLoading(false));
        event.preventDefault();
    }

    return (
        <div className="container">
            <div className="topnav">
                <form onSubmit={(e) => handleSubmit(e, null)}>
                    <input type="text" placeholder="Name of Character" id='name' name='name' value={characterName} onChange={handleChange} className="search" />

                    <input type="submit" className="btn" value="Search" />
                </form>
            </div>
            {isLoading && <h2>is loading...</h2>}
            {characters.results?.length > 0 ?
                <div className="float-container">
                    <div className="float-selected">
                        <CharacterSelected data={characterSelected} />
                    </div>
                    <div className="float-characters">
                        {characters.results?.map(character => <Character data={character} setCharacterSelected={setCharacterSelected} />)}
                        <div className="pagination">
                            <button className="btn" onClick={(e) => handleSubmit(e, characters.info.prev)} disabled={!characters.info.prev}>prev</button>
                            <button className="btn" onClick={(e) => handleSubmit(e, characters.info.next)}>next</button>
                            <span>{`Total Results: ${characters.info.count}`}</span>
                            <div></div>
                        </div>

                    </div>
                </div>
                : <h2>Not results found</h2>
            }
        </div>

    )
}

export default Dashboard;