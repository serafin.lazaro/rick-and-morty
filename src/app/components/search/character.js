import { React, useState } from 'react';

const Character = (props) => {
    const [selected, setSelected] = useState();
    const onSelected = () => {
        setSelected(true);
        props.setCharacterSelected(props.data);
    };

    return (
        <div className="character-card" onClick={onSelected} id={props.data.id} >
            <img src={props.data.image} className="character-card-image" alt="{props.data.name}" />
            <div className="character-card-content">
                <h3 className="character-card-title">{props.data.name}</h3>
            </div>
        </div>
    )
}

export default Character;