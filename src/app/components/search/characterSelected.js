import { React } from 'react'

const notFoundAvatar = 'https://rickandmortyapi.com/api/character/avatar/19.jpeg';

const CharacterSelected = (props) => {
    return (
        <div>
            {props.data?.id ? <div className="character-selected-card" id={props.data.id}>
                <img src={props.data.image} className="character-selected-image" alt={props.data.name}/>
                <div className="character-card-content">
                    <h3 className="character-card-content-title">{props.data.name}</h3>
                        <p className="character-card-content-text">{`Status: ${props.data.status}`}</p>
                        <p className="character-card-content-text">{`Species: ${props.data.species}`}</p>
                    <p className="character-card-content-text">{`Gender: ${props.data.gender}`}</p>
                    <p className="character-card-content-text">{`Origin Name: ${props.data.origin.name}`}</p>
                </div>
            </div>:
            <div className="character-selected-card">
                <img src={`${notFoundAvatar}`} className="character-selected-image" alt="Not selected"/>
                <div className="character-card-content">
                    <h3 className="character-card-content-title">Not selected</h3>
                </div>
                
            </div>
            }
        </div>
    )
}

export default CharacterSelected;