import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Dashboard from './app/components/search/dashboard';
import { SearchState } from "./app/contexts/search/state";
import { AlertState } from "./app/contexts/alerts/state";

function App() {
  return (
    <Router>
      <div>
          <SearchState>
            <AlertState>
            <Switch>
              <Route exact path="/">
                <Dashboard />
              </Route>
            </Switch>
            </AlertState>
          </SearchState>
      </div>
    </Router>
  );
}

export default App;
